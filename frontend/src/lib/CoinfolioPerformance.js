import { computed } from 'vue';

export function enhanceHoldingsEntry(holdingEntry, store) {
    const amount = holdingEntry.holdings.amount;
    const totalPaid = holdingEntry.holdings.totalPaid;

    let currentValue = computed(() => store.byCurrency(holdingEntry.currency.tickerSymbol));
    const currentTotalValue = computed(() => currentValue.value * amount);

    let result = {
        amount: amount,
        currency: holdingEntry.currency,
        currentValue: currentValue,
        totalValue: currentTotalValue,
        totalPaid: totalPaid,
        totalProfit: computed(() => currentTotalValue.value - totalPaid),
        totalProfitPercent: computed(() => ((currentTotalValue.value / totalPaid - 1) * 100).toFixed(2)),
        totalProfitPositive: computed(() => currentTotalValue.value - totalPaid >= 0),
    };

    return result;
}

export function enhanceHoldingsResult(holdingsResult, store) {
    let result = {
        holdings: [],
        overalls: {
            totalValue: 0,
            paid: 0,
            profit: 0,
            profitPercent: 0,
            isProfitPositive: true,
        }
    };

    for(const resultEntry of holdingsResult) {
        const holdingsEntry = enhanceHoldingsEntry(resultEntry, store);

        result.holdings.push(holdingsEntry);

        result.overalls.paid += holdingsEntry.totalPaid;
    }


    // Calculate overalls only based on values that do not contain NaN
    const validEntries = result.holdings.filter((candidate) => !isNaN(candidate.currentValue.value))

    result.overalls.totalValue = computed(() => validEntries.reduce((sum, current) => sum + current.totalValue.value, 0));
    result.overalls.profit = computed(() => result.overalls.totalValue.value - result.overalls.paid);
    result.overalls.profitPercent = computed(() => ((result.overalls.totalValue.value / result.overalls.paid - 1) * 100).toFixed(2));
    result.overalls.isProfitPositive = computed(() => result.overalls.profit.value >= 0);

    return result;
}