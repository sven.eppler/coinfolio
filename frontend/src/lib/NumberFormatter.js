export function currencyFormat(value) {
    let minimumFractionDigits = 2;
    let maximumFractionDigits = 2;
    if(value < 1) {
        minimumFractionDigits = 4;
        maximumFractionDigits = 4;
    }
    if(value < 0.0001) {
        minimumFractionDigits = 8;
        maximumFractionDigits = 8;
    }

    return new Intl.NumberFormat('de-DE', { minimumFractionDigits, maximumFractionDigits }).format(value);
}

export function moneyFormat(value) {
    let minimumFractionDigits = 2;
    let maximumFractionDigits = 2;

    if(value > 0) {
        if(value < 1) {
            minimumFractionDigits = 4;
            maximumFractionDigits = 4;
        }
        if(value < 0.0001) {
            minimumFractionDigits = 8;
            maximumFractionDigits = 8;
        }
    }

    return new Intl.NumberFormat('de-DE', { minimumFractionDigits, maximumFractionDigits }).format(value);
}

export function numberFormat(value) {
    return new Intl.NumberFormat('de-DE').format(value);
}
