import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import CreateCoinfolioView from '../views/CreateCoinfolioView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/create-coinfolio',
      name: 'create-coinfolio',
      component: CreateCoinfolioView,
    },
    {
      path: '/coinfolio/:id',
      name: 'coinfolio',
      component: () => import('../components/CoinFolio.vue'),
      props: {
        "coinfolioId": 4711,
      }
    },
    {
      path: '/add-coinfolio',
      name: 'add-coinfolio',
      component: () => import('../views/AddCoinFolioView.vue'),
    },
    {
      // TODO ownerToken via URL param is big nononono!
      path: '/record-transaction/:coinfolioId/:ownerToken',
      name: "record-transaction",
      component: () => import("../views/RecordTransactionView.vue"),
    },
    {
      // TODO ownerToken via URL param is big nononono!
      path: '/view-transactions/:coinfolioId/:ownerToken/:currency',
      name: "view-transactions",
      component: () => import("../views/TransactionListView.vue"),
    }
  ]
})

export default router
