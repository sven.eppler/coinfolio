import { defineStore } from 'pinia';

export const useCoinfoliosStore = defineStore('coinfolios', {
    state: () => {
        return {
            coinfolios: []
        }
    }
});