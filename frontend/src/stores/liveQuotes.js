import { defineStore } from 'pinia';

export const useLiveQuotesStore = defineStore('livequotes', {
    state: () => {
        return {
            loaded: 0,
            currencies: {}
        }
    },
    getters: {
        byCurrency: (state) => {
            return (currency) => {
                if(state.loaded) {
                    if(state.currencies[currency]) {
                        return state.currencies[currency].EUR;
                    }
                }
                
                console.log(`WARNING: Tried to get current price for currency=${currency} but is not in the store!`);
                return NaN;
            }
        }
    }
});