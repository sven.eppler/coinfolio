import { expect, test } from 'vitest'

import { enhanceHoldingsEntry, enhanceHoldingsResult } from '../../src/lib/CoinfolioPerformance';

const mockStore = {
    byCurrency: (currency) => {
        if(currency == "ADA") {
            return 1;
        }

        if(currency == "BTC") {
            return 1;
        }

        return 0;
    },
};


test("100% profit", () => {
    let holdingsEntry = {
        currency: {
            tickerSymbol: "ADA",
        },
        holdings: {
            amount: 100,
            totalPaid: 50,
        }
    };

    let result = enhanceHoldingsEntry(holdingsEntry, mockStore);

    expect(result.totalPaid).toBe(50);
    expect(result.totalValue.value).toBe(100);
    expect(result.totalProfit.value).toBe(50);
    expect(result.totalProfitPercent.value).toBe("100.00");
    expect(result.totalProfitPositive.value).toBe(true);
    expect(result.amount).toBe(holdingsEntry.holdings.amount);
    expect(result.totalPaid).toBe(holdingsEntry.holdings.totalPaid);
});

test("-90% profit", () => {
    let holdingsEntry = {
        currency: {
            tickerSymbol: "ADA",
        },
        holdings: {
            amount: 100,
            totalPaid: 1000,
        },
    };

    let result = enhanceHoldingsEntry(holdingsEntry, mockStore);

    expect(result.totalPaid).toBe(1000);
    expect(result.totalValue.value).toBe(100);
    expect(result.totalProfit.value).toBe(-900);
    expect(result.totalProfitPercent.value).toBe("-90.00");
    expect(result.totalProfitPositive.value).toBe(false);
    expect(result.amount).toBe(holdingsEntry.holdings.amount);
    expect(result.totalPaid).toBe(holdingsEntry.holdings.totalPaid);
});

test("0% profit", () => {
    let holdingsEntry = {
        currency: {
            tickerSymbol: "ADA",
        },
        holdings: {
            amount: 100,
            totalPaid: 100,
        },
    };

    let result = enhanceHoldingsEntry(holdingsEntry, mockStore);

    expect(result.totalPaid).toBe(100);
    expect(result.totalValue.value).toBe(100);
    expect(result.totalProfit.value).toBe(0);
    expect(result.totalProfitPercent.value).toBe("0.00");
    expect(result.totalProfitPositive.value).toBe(true);
    expect(result.amount).toBe(holdingsEntry.holdings.amount);
    expect(result.totalPaid).toBe(holdingsEntry.holdings.totalPaid);
});

test("overall profit +100%", () => {
    let holdingsApiResult = [
        {
            currency: {
                tickerSymbol: "ADA",
            },
            holdings: {
                amount: 100,
                totalPaid: 50,
            },
        },
        {
            currency: {
                tickerSymbol: "BTC",
            },
            holdings: {
                amount: 100,
                totalPaid: 50,
            },
        },
    ];

    let result = enhanceHoldingsResult(holdingsApiResult, mockStore);

    expect(result.overalls.paid).toBe(100);
    expect(result.overalls.totalValue.value).toBe(200);
    expect(result.overalls.profit.value).toBe(100);
    expect(result.overalls.profitPercent.value).toBe("100.00");
    expect(result.overalls.isProfitPositive.value).toBe(true);
});

test("overall profit -50%", () => {
    let holdingsApiResult = [
        {
            currency: {
                tickerSymbol: "ADA",
            },
            holdings: {
                amount: 100,
                totalPaid: 200,
            },
        },
        {
            currency: {
                tickerSymbol: "BTC",
            },
            holdings: {
                amount: 100,
                totalPaid: 200,
            },
        },
    ];

    let result = enhanceHoldingsResult(holdingsApiResult, mockStore);

    expect(result.overalls.paid).toBe(400);
    expect(result.overalls.totalValue.value).toBe(200);
    expect(result.overalls.profit.value).toBe(-200);
    expect(result.overalls.profitPercent.value).toBe("-50.00");
    expect(result.overalls.isProfitPositive.value).toBe(false);
});

test("overall profit 0%", () => {
    let holdingsApiResult = [
        {
            currency: {
                tickerSymbol: "ADA",
            },
            holdings: {
                amount: 100,
                totalPaid: 100,
            },
        },
        {
            currency: {
                tickerSymbol: "BTC",
            },
            holdings: {
                amount: 100,
                totalPaid: 100,
            },
        },
    ];

    let result = enhanceHoldingsResult(holdingsApiResult, mockStore);

    expect(result.overalls.paid).toBe(200);
    expect(result.overalls.totalValue.value).toBe(200);
    expect(result.overalls.profit.value).toBe(0);
    expect(result.overalls.profitPercent.value).toBe("0.00");
    expect(result.overalls.isProfitPositive.value).toBe(true);
});