#!/usr/bin/env bash

set -e
set -x

# Change buildah storage driver to vfs
export STORAGE_DRIVER=vfs

COINFOLIO_VERSION=${CI_COMMIT_TAG}
API_IMAGE_NAME=${CI_REGISTRY_IMAGE}/api:${COINFOLIO_VERSION}
LIVEQUOTES_IMAGE_NAME=${CI_REGISTRY_IMAGE}/livequotes:${COINFOLIO_VERSION}
FRONTEND_IMAGE_NAME=${CI_REGISTRY_IMAGE}/frontend:${COINFOLIO_VERSION}

buildah login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}



buildah info

echo "Going to build container image '${API_IMAGE_NAME}'"
buildah build -f src/api/Dockerfile -t ${API_IMAGE_NAME} .

echo "Going to build container image '${LIVEQUOTES_IMAGE_NAME}'"
buildah build -f src/livequotes/Dockerfile -t ${LIVEQUOTES_IMAGE_NAME} .

echo "Going to build container image '${FRONTEND_IMAGE_NAME}'"
pushd frontend/
buildah build --build-arg VITE_COINFOLIO_API_BASE_ADDRESS=https://api.coinfolio.sveneppler.de -f Dockerfile -t ${FRONTEND_IMAGE_NAME} .
popd

echo "Pushing images to registry"
buildah push ${API_IMAGE_NAME}
buildah push ${LIVEQUOTES_IMAGE_NAME}
buildah push ${FRONTEND_IMAGE_NAME}