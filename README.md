# CoinFolio

An anonymous crypto currency portfolio tracker.

## Concept
### CoinFolio

A `CoinFolio` tracks one or many `Holdings`.

### Holding

A `Holding` represents the amount of an `Asset` someone holds, determined by a list of `Transactions`.

## Commands

- RecordBuyTransaction (BuyTransactionRecorded)
    - MomentOfTransaction
    - Pair
    - Amount
    - UnitPrice
    - Fee
    
- RecordSellTransaction (SellTransactionRecorded)
    - MomentOfTransaction
    - Pair
    - Amount
    - UnitPrice
    - Fee

### Types
- Currency
    - ID
    - TickerSymbol
    - Icon
    - Name

- Pair
    - BaseCurrency
    - QuoteCurrency