namespace CoinFolio.LiveQuotes;

public class KrakenService : IHostedService
{
    KrakenClient _client;
    public KrakenService(IServiceProvider serviceProvider)
    {
        _client = new KrakenClient(
            serviceProvider.GetRequiredService<ILogger<KrakenClient>>(),
            serviceProvider.GetRequiredService<IConfiguration>()
        );
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        return _client.StartAsync(cancellationToken);
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        _client.Stop();
        return Task.CompletedTask;
    }
}