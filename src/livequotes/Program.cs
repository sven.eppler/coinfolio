﻿using CoinFolio.LokiLogging;

namespace CoinFolio.LiveQuotes;

public class Program
{
    public static async Task Main(string[] args) {
        IHost host = Host.CreateDefaultBuilder(args)
            .ConfigureLogging((hostBuilder, loggingBuilder) => {
                loggingBuilder.AddOpenTelemetryLogging(hostBuilder.Configuration);
            })
            .ConfigureServices(services =>
            {
                services.AddHostedService<KrakenService>();
            })
            .Build();

        await host.RunAsync();
    }
}