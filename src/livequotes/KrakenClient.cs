using System.Globalization;
using System.Net.WebSockets;
using System.Text;
using System.Text.Json.Nodes;

using InfluxDB.Client;
using InfluxDB.Client.Writes;

using Json.Path;

namespace CoinFolio.LiveQuotes;

public class KrakenClient
{
    record TradeRecord(string Pair, decimal Price, decimal Volume, DateTimeOffset Timestamp);

    private readonly Dictionary<string, string> _tickerTranslationMap = new Dictionary<string, string>() {
        {"XBT", "BTC"},
        {"LUNA", "LUNC"},
        {"LUNA2", "LUNA"},
        {"XDG", "DOGE"},
    };

    ILogger<KrakenClient> _logger;
    private readonly IConfiguration _configuration;
    CancellationTokenSource _stopRequested = new CancellationTokenSource();

    public KrakenClient(ILogger<KrakenClient> logger, IConfiguration configuration)
    {
        _logger = logger;
        _configuration = configuration;
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        var combinedCancellationToken = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken, _stopRequested.Token);

        var euroPairs = await GetEuroPairsFromKraken();

        while (!combinedCancellationToken.IsCancellationRequested)
        {
            try
            {
                using var influxdb = new InfluxDBClient(_configuration.GetValue<string>("influxdb:address"), _configuration.GetValue<string>("influxdb:accesstoken"));
                using var ws = new ClientWebSocket();

                await ws.ConnectAsync(new Uri("wss://ws.kraken.com/"), cancellationToken);
                // TODO this is sometimes to small. Implement a real WebSocket-Reader
                byte[] buf = new byte[1056];

                while (ws.State == WebSocketState.Open)
                {
                    var msg = await Receive(ws, cancellationToken);
                    _logger.LogTrace("Received message: " + msg);

                    if(msg.Contains("\"status\":\"error\"")) {
                        _logger.LogError("Kraken WebSocket errorMessage={errorMessage}", msg);
                    }

                    if (msg.Contains("heartbeat"))
                    {
                        continue;
                    }

                    if (msg.Contains("systemStatus") && msg.Contains("online"))
                    {
                        // var pairs = new[]  { "XBT/EUR", "ADA/EUR", "LTC/EUR", "ARB/EUR", "ETH/EUR", "DOT/EUR", "DOGE/EUR", "WAVES/EUR", "SHIB/EUR", "ALGO/EUR", "TRX/EUR", "BAT/EUR", "XTZ/EUR", "SGB/EUR", "BLUR/EUR", "MINA/EUR", "LUNA/EUR", "FLR/EUR", "LUNA2/EUR", "ATOM/EUR", "XLM/EUR", "ACA/EUR" };
                        _logger.LogInformation("Going to subscribe to {pairCount} pairs.", euroPairs.Count());
                        var joinedPairs = string.Join(",", euroPairs.Select(p => '"' + p + '"'));
                        var subscribeMsg = Encoding.UTF8.GetBytes("{\"event\": \"subscribe\",\"pair\": [" + joinedPairs + "],\"subscription\": {\"name\": \"trade\"}}");
                        await ws.SendAsync(subscribeMsg, WebSocketMessageType.Text, true, cancellationToken);
                    }
                    else
                    {
                        var trades = ParseTradeMessage(msg);
                        foreach (var trade in trades)
                        {
                            var splitResult = trade.Pair.Split("/", 2);
                            var baseCurrencyTickerSymbol = TranslateTickerSymbol(splitResult[0]);
                            var quoteCurrencyTickerSymbol = TranslateTickerSymbol(splitResult[1]);
                            var sanitizedPair = string.Join("-", baseCurrencyTickerSymbol, quoteCurrencyTickerSymbol).ToLower();

                            using var writeAPI = influxdb.GetWriteApi();
                            var builder = PointData.Measurement(sanitizedPair).Tag("exchange", "kraken");
                            var datapoint = builder.Field("value", trade.Price).Timestamp(trade.Timestamp, InfluxDB.Client.Api.Domain.WritePrecision.Ms);
                            writeAPI.WritePoint(datapoint, "live-quotes", _configuration.GetValue<string>("influxdb:organization"));
                        }
                    }
                }
            }
            catch (TaskCanceledException ex) {
                // normal shutdown.
                return;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "KrakenClient MainLoop Error");
                await Task.Delay(1000);
            }
        }
    }

    public async Task<IEnumerable<string>> GetEuroPairsFromKraken() {
        var client = new HttpClient();
        var res = await client.GetAsync("https://api.kraken.com/0/public/AssetPairs");
        
        if(res.StatusCode != System.Net.HttpStatusCode.OK) {
            throw new Exception("Failed to retrieve tradable AssetPairs from kraken api!");
        }


        var body = await res.Content.ReadAsStringAsync();
        var json = JsonNode.Parse(body);
        var pointer = JsonPath.Parse("$.result[*].wsname");

        var result = pointer.Evaluate(json);


        var euroPairs = new List<string>();
        foreach(var match in result.Matches) {
            var websocketPairName = match.Value.GetValue<string>();
            if(websocketPairName.EndsWith("/EUR")) {
                euroPairs.Add(websocketPairName);
            }
        }

        return euroPairs;
    }

    public void Stop() {
        _stopRequested.Cancel();
    }

    static async Task<string> Receive(ClientWebSocket socket, CancellationToken cancellationToken)
    {
        var buffer = new ArraySegment<byte>(new byte[2048]);
        do
        {
            WebSocketReceiveResult result;
            using (var ms = new MemoryStream())
            {
                do
                {
                    result = await socket.ReceiveAsync(buffer, cancellationToken);
                    ms.Write(buffer.Array, buffer.Offset, result.Count);
                } while (!result.EndOfMessage);

                if (result.MessageType == WebSocketMessageType.Close)
                    return "";

                ms.Seek(0, SeekOrigin.Begin);
                using var reader = new StreamReader(ms, Encoding.UTF8);
                return await reader.ReadToEndAsync();
            }
        } while (true);
    }

    static IEnumerable<TradeRecord> ParseTradeMessage(string msg)
    {
        var rootNode = JsonNode.Parse(msg);

        if (rootNode is not JsonArray)
        {
            return Enumerable.Empty<TradeRecord>();
        }

        var pair = rootNode[3] as JsonValue;
        if (pair is null)
        {
            return Enumerable.Empty<TradeRecord>();
        }

        var tradeData = rootNode[1];

        if (tradeData is not JsonArray)
        {
            return Enumerable.Empty<TradeRecord>();
        }

        var result = new List<TradeRecord>();

        foreach (var trade in (JsonArray)tradeData)
        {
            if (trade is JsonArray)
            {
                var tradePair = pair.GetValue<string>();
                var tradePrice = Decimal.Parse(trade[0].GetValue<string>(), CultureInfo.InvariantCulture);
                var tradeVolume = Decimal.Parse(trade[1].GetValue<string>(), CultureInfo.InvariantCulture);
                var blubb = Convert.ToInt64(Convert.ToDouble(trade[2].GetValue<string>(), CultureInfo.InvariantCulture));
                var timestamp = DateTimeOffset.FromUnixTimeSeconds(blubb);

                result.Add(new(tradePair, tradePrice, tradeVolume, timestamp));
            }

        }

        return result;
    }

    public string TranslateTickerSymbol(string krakenSymbol) {
        if(_tickerTranslationMap.TryGetValue(krakenSymbol, out var translatedSymbol)) {
            return translatedSymbol;
        }

        return krakenSymbol;
    }
}