﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using OpenTelemetry.Logs;
using OpenTelemetry.Resources;

namespace CoinFolio.LokiLogging;

public static class ServiceCollectionExtensions {
    public static void AddOpenTelemetryLogging(this ILoggingBuilder loggingBuilder, IConfiguration configuration) {
        var otelConfig = configuration.GetSection("OpenTelemetry");

        var enableOtelLogging = otelConfig.GetValue<bool>("Logging:Enable", false);

        if(!enableOtelLogging) {
            return;
        }

        loggingBuilder.AddOpenTelemetry(x => {
            x.AddOtlpExporter(c => {
                var lokiOtlpIngestEndpoint = new Uri(otelConfig.GetValue<string>("Logging:HttpEndpoint", ""));
                c.Endpoint = lokiOtlpIngestEndpoint;

                c.Protocol = OpenTelemetry.Exporter.OtlpExportProtocol.HttpProtobuf;

                var username = otelConfig.GetValue<string>("Logging:HttpUser", "");
                var password = otelConfig.GetValue<string>("Logging:HttpPassword", "");
                var comboString = username + ":" + password;
                var comboStringBytes = System.Text.Encoding.UTF8.GetBytes(comboString);
                var authHeaderValue = System.Convert.ToBase64String(comboStringBytes);

                c.HttpClientFactory = () => {
                    HttpClient client = new HttpClient();
                    client.DefaultRequestHeaders.Add("Authorization", $"Basic {authHeaderValue}");
                    return client;
                };
            });

            var serviceName = otelConfig.GetValue<string>("Logging:ServiceName", "");
            var serviceNamespace = otelConfig.GetValue<string>("Logging:ServiceNamespace", "CoinFolio");
            x.SetResourceBuilder(ResourceBuilder.CreateDefault().AddService(serviceName, serviceNamespace));
            x.IncludeFormattedMessage = true;
        });

    }
}