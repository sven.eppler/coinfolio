using CoinFolio.Services;

using Eventuous;

using InfluxDB.Client;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace CoinFolio.Controllers;

public class CoinfolioQueriesController : ControllerBase {
    private readonly IAggregateStore _store;
    private readonly IInfluxDBClient _influxDB;
    private readonly IConfiguration _configuration;
    private readonly IKrakenPairService _krakenPairService;
    private readonly IMemoryCache _memoryCache;
    private readonly ILogger<KrakenPairService> _logger;

    // TODO they are identical, and many more will also be identical...
    public record QueryInfoModel(string CoinfolioId, string OwnerToken) { }
    public record QueryHoldingsModel(string CoinfolioId, string OwnerToken) { }
    public record QueryTransactionsModel(string CoinfolioId, string OwnerToken, string Currency) { }

    public CoinfolioQueriesController(IAggregateStore store,
                                      IInfluxDBClient influxDB,
                                      IConfiguration configuration,
                                      IKrakenPairService krakenPairService,
                                      IMemoryCache memoryCache,
                                      ILogger<KrakenPairService> logger)
    {
        _store = store;
        _influxDB = influxDB;
        _configuration = configuration;
        _krakenPairService = krakenPairService;
        _memoryCache = memoryCache;
        _logger = logger;
    }

    [HttpPost("/queries/info")]
    public async Task<IActionResult> QueryInfo([FromBody] QueryInfoModel model, CancellationToken cancellationToken) {
        var id = StreamName.For<Domain.CoinFolio>(model.CoinfolioId);
        var coinfolio = await _store.Load<Domain.CoinFolio>(id, cancellationToken);

        if(model.OwnerToken != coinfolio.State.OwnerToken) {
            return BadRequest("invalid ownerToken");
        }

        return new JsonResult(new {
            coinfolioId = coinfolio.State.Id,
            title = coinfolio.State.Title,
            isClosed = coinfolio.State.IsClosed,
        });
    }

    [HttpPost("/queries/holdings/")]
    public async Task<IActionResult> QueryHoldings([FromBody] QueryHoldingsModel model, CancellationToken cancellationToken) {
        var id = StreamName.For<Domain.CoinFolio>(model.CoinfolioId);
        var coinfolio = await _store.Load<Domain.CoinFolio>(id, cancellationToken);

        if(model.OwnerToken != coinfolio.State.OwnerToken) {
            return BadRequest("invalid ownerToken");
        }

        return new JsonResult(coinfolio.State.Holdings.Select(pair => new { currency = pair.Key, holdings = pair.Value, currentValue = 4711.12m }));
    }

    [HttpGet("/queries/live-quotes")]
    public async Task<IActionResult> QueryLiveQuotesAsync() {

        var flux = "from(bucket:\"live-quotes\") |> range(start: 0) |> last()";
        var fluxTables = await _influxDB.GetQueryApi().QueryAsync(flux, _configuration.GetValue<string>("influxdb:organization"));

        var liveQuotes = new Dictionary<string, Dictionary<string, decimal>>();

        foreach(var table in fluxTables) {
            foreach(var record in table.Records) {
                var measurement = record.GetMeasurement();
                var value = record.GetValue();

                var currency = measurement.Split("-", 2)[0];

                // TODO support multiple currencies
                if(measurement == "xbt-usd") {
                    continue;
                }
                // TODO fix currency name in kraken-client.
                if(currency == "xbt") {
                    currency = "btc";
                }

                currency = currency.ToUpper();
                liveQuotes[currency] = new Dictionary<string, decimal> {{ "EUR", Convert.ToDecimal(value) }};
            }
        }

        return new JsonResult(liveQuotes);
    }

    [HttpGet("/queries/supported-pairs")]
    public async Task<IActionResult> SupportedCurrencies() {
        var cacheMiss = !_memoryCache.TryGetValue("supported-pairs", out var supportedPairs);

        if(cacheMiss) {
            _logger.LogInformation("Cache miss for supported-pairs. Refetching from Kraken API.");
            var allPairs = await _krakenPairService.GetPairs();

            supportedPairs = allPairs
                .Where(p => p.QuoteCurrency == "EUR")
                .OrderBy(p => p.TickerSymbol)
                .Select(p => p.TickerSymbol);
            
            _memoryCache.Set("supported-pairs", supportedPairs, TimeSpan.FromHours(1));
        }
        else {
            _logger.LogInformation("Serving supported-pairs from cache.");
        }

        return new JsonResult(supportedPairs);
    }

    [HttpPost("/queries/transactions/")]
    public async Task<IActionResult> TransactionsByCurrencyAsync([FromBody] QueryTransactionsModel model, CancellationToken cancellationToken) {
        var id = StreamName.For<Domain.CoinFolio>(model.CoinfolioId);
        var coinfolio = await _store.Load<Domain.CoinFolio>(id, cancellationToken);

        if(model.OwnerToken != coinfolio.State.OwnerToken) {
            return BadRequest("invalid ownerToken");
        }

        return new JsonResult(coinfolio.State.Transactions);
    }
}