using CoinFolio.Services;

using Eventuous;

using Microsoft.AspNetCore.Mvc;

using Prometheus;

namespace CoinFolio.Controllers;

public class CoinFolioCommandsController : ControllerBase
{
    private readonly IApplicationService<Domain.CoinFolio> _service;
    private readonly IKrakenPairService _krakenPairService;
    private readonly Counter OpenedCoinFolios = Metrics.CreateCounter("coinfolio_opened_total", "Total count of opened CoinFolios");
    private readonly Counter ClosedCoinFolios = Metrics.CreateCounter("coinfolio_closed_total", "Total count of closed CoinFolios");
    private readonly Counter RecordedBuyTransactions = Metrics.CreateCounter("coinfolio_recorded_buy_transactions_total", "Total count or recorded buy transactions");
    private readonly Counter RecordedSellTransactions = Metrics.CreateCounter("coinfolio_recorded_sell_transactions_total", "Total count or recorded sell transactions");

    public record OpenCoinfolioModel(string Title) { }
    public record CloseCoinfolioModel(string CoinfolioId, string OwnerToken, string LastWords) { }
    public record RecordBuyTransactionModel(string CoinFolioId, string OwnerToken, string Pair, decimal Amount, decimal UnitPrice, decimal Fee, DateTime MomentOfTransaction, string Exchange, string Notes) { }
    public record RecordSellTransactionModel(string CoinFolioId, string OwnerToken, string Pair, decimal Amount, decimal UnitPrice, decimal Fee, DateTime MomentOfTransaction, string Exchange, string Notes) { }

    public CoinFolioCommandsController(IApplicationService<Domain.CoinFolio> service, IKrakenPairService krakenPairService)
    {
        _service = service;
        _krakenPairService = krakenPairService;
    }

    [HttpPost("/commands/open-coinfolio")]
    public async Task<IActionResult> OpenCoinfolio([FromBody] OpenCoinfolioModel model, CancellationToken cancellationToken)
    {
        var domainCommand = new Domain.Commands.OpenCoinfolio(Guid.NewGuid().ToString(), model.Title);

        var result = await _service.Handle(domainCommand, cancellationToken);

        OpenedCoinFolios.Inc();

        return HandleResultJson<Domain.CoinFolioState>(
            result,
            state => new { state.Id, state.OwnerToken }
        );
    }

    [HttpPost("/commands/close-coinfolio")]
    public async Task<IActionResult> CloseCoinfolio([FromBody] CloseCoinfolioModel model, CancellationToken cancellationToken)
    {
        var command = new Domain.Commands.CloseCoinfolio(model.CoinfolioId, model.OwnerToken, model.LastWords);

        var result = await _service.Handle(command, cancellationToken);

        ClosedCoinFolios.Inc();

        return HandleResult(result);
    }

    [HttpPost("/commands/record-buy-transaction")]
    public async Task<IActionResult> RecordBuyTransaction([FromBody] RecordBuyTransactionModel model, CancellationToken cancellationToken)
    {
        var pair = await PairLookup(model.Pair);

        var cmd = new Domain.Commands.RecordBuyTransaction(
            model.CoinFolioId,
            model.OwnerToken,
            pair,
            model.Amount,
            model.UnitPrice,
            model.Fee,
            model.MomentOfTransaction,
            model.Exchange,
            model.Notes
        );

        var result = await _service.Handle(cmd, cancellationToken);

        RecordedBuyTransactions.Inc();

        return HandleResult(result);
    }

    [HttpPost("/commands/record-sell-transaction")]
    public async Task<IActionResult> RecordSellTransaction([FromBody] RecordSellTransactionModel model, CancellationToken cancellationToken)
    {
        var pair = await PairLookup(model.Pair);

        var cmd = new Domain.Commands.RecordSellTransaction(
            model.CoinFolioId,
            model.OwnerToken,
            pair,
            model.Amount,
            model.UnitPrice,
            model.Fee,
            model.MomentOfTransaction,
            model.Exchange,
            model.Notes
        );

        var result = await _service.Handle(cmd, cancellationToken);

        RecordedSellTransactions.Inc();

        return HandleResult(result);
    }

    public IActionResult HandleResult(Eventuous.Result result)
    {
        return HandleResult<object>(result, (_) => Ok());
    }

    public IActionResult HandleResult<T>(Eventuous.Result result, Func<T, IActionResult> okHandler)
        where T : class
    {
        IActionResult nullWrapper(object? innerState)
        {
            if (innerState is null)
            {
                throw new ArgumentNullException("state is null");
            }

            if (innerState is not T castedState)
            {
                throw new InvalidOperationException($"state is not of expected type {typeof(T).Name}");
            }

            return okHandler(castedState);
        }

        return result switch
        {
            Eventuous.OkResult ok => nullWrapper(ok.State),
            Eventuous.ErrorResult error => BadRequest(error.ErrorMessage),
            _ => throw new InvalidOperationException("unhandled result type!")
        };
    }

    public IActionResult HandleResultJson<T>(Eventuous.Result result, Func<T, object> handler)
        where T : class
    {
        return HandleResult<T>(result, (state) => new JsonResult(handler(state)));
    }

    // TODO this needs to be "designed" in some way later on.
    public async Task<Domain.Pair> PairLookup(string pairCandidate) {
        var supportedPairs = await _krakenPairService.GetPairs();
        var euroPairs = supportedPairs.Where(p => p.QuoteCurrency == "EUR");
        
        var matchedPair = euroPairs.Where(p => p.TickerSymbol == pairCandidate).FirstOrDefault();

        if(matchedPair is null) {
            throw new Exception($"Invalid pair={pairCandidate}");
        }

        return new Domain.Pair(
            new Domain.Currency(matchedPair.BaseCurrency, matchedPair.BaseCurrency),
            new Domain.Currency(matchedPair.QuoteCurrency, matchedPair.QuoteCurrency)
        );
    }
}
