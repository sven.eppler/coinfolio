namespace CoinFolio.Domain;

public static class Exceptions {
    [System.Serializable]
    public class CannotOverSellException : System.Exception
    {
        public CannotOverSellException(Currency currency, decimal sellAmount, decimal availableAmount) : base($"Tried to sell '{sellAmount}' of '{currency}' but only has '{availableAmount}' available") {
            Data.Add("currencyName", currency.Name);
            Data.Add("currencyTickerSymbol", currency.TickerSymbol);
            Data.Add("availableBalance", availableAmount);
            Data.Add("sellAmount", sellAmount);
        }
        protected CannotOverSellException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }

    [System.Serializable]
    public class NotHoldingException : System.Exception
    {
        public NotHoldingException(Currency c) : base($"You don't hold any `{c}` you can sell.") {
            Data.Add("currencyName", c.Name);
            Data.Add("currencyTickerSymbol", c.TickerSymbol);
        }
        protected NotHoldingException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }

    [System.Serializable]
    public class CoinfolioClosedException : System.Exception
    {
        public CoinfolioClosedException() { }
        public CoinfolioClosedException(string message) : base(message) { }
        public CoinfolioClosedException(string message, System.Exception inner) : base(message, inner) { }
        protected CoinfolioClosedException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}