namespace CoinFolio.Domain;

public class SupportedCurrencies {
    public static readonly Domain.Currency EUR = new ("Euro", "EUR");
    public static readonly Domain.Currency USD = new ("US Dollar", "USD");

    public static readonly Domain.Currency BTC = new ("Bitcoin", "BTC");
    public static readonly Domain.Currency LTC = new ("Litecoin", "LTC");
    public static readonly Domain.Currency ADA = new ("Cardano", "ADA");
    public static readonly Domain.Currency SHIB = new ("Shiba Inu", "SHIB");
    public static readonly Domain.Currency DOGE = new ("Dogecoin", "DOGE");
    public static readonly Domain.Currency TRX = new ("TRON", "TRX");
    public static readonly Domain.Currency ARB = new ("Arbitrum", "ARB");
    public static readonly Domain.Currency ETH = new ("Ethereum", "ETH");
    public static readonly Domain.Currency WAVES = new ("WAVES", "WAVES");
    public static readonly Domain.Currency ALGO = new ("Algorand", "ALGO");
    public static readonly Domain.Currency BAT = new ("Basic Attention Coin", "BAT");
    public static readonly Domain.Currency XTZ = new ("Tezos", "XTZ");
    public static readonly Domain.Currency ACA = new ("Acala", "ACA");
    public static readonly Domain.Currency SGB = new ("Songbird", "SGB");
    public static readonly Domain.Currency MINA = new ("Mina", "MINA");
    public static readonly Domain.Currency FLR = new ("Flare Network", "FLR");
    public static readonly Domain.Currency DOT = new ("Polkadot", "DOT");
    public static readonly Domain.Currency ATOM = new ("Cosmos", "ATOM");
    public static readonly Domain.Currency XLM = new ("Stellar Lumen", "XLM");
}

public class SupportedPairs {
    private static readonly Domain.Currency EUR = new Domain.Currency("Euro", "EUR");

    private static List<Domain.Pair> _supportedPairs = new List<Domain.Pair>() {
        new Domain.Pair(SupportedCurrencies.BTC, SupportedCurrencies.EUR),
        new Domain.Pair(SupportedCurrencies.LTC, SupportedCurrencies.EUR),
        new Domain.Pair(SupportedCurrencies.ADA, SupportedCurrencies.EUR),
        new Domain.Pair(SupportedCurrencies.SHIB, SupportedCurrencies.EUR),
        new Domain.Pair(SupportedCurrencies.DOGE, SupportedCurrencies.EUR),
        new Domain.Pair(SupportedCurrencies.TRX, SupportedCurrencies.EUR),
        new Domain.Pair(SupportedCurrencies.ARB, SupportedCurrencies.EUR),
        new Domain.Pair(SupportedCurrencies.ETH, SupportedCurrencies.EUR),
        new Domain.Pair(SupportedCurrencies.WAVES, SupportedCurrencies.EUR),
        new Domain.Pair(SupportedCurrencies.ALGO, SupportedCurrencies.EUR),
        new Domain.Pair(SupportedCurrencies.BAT, SupportedCurrencies.EUR),
        new Domain.Pair(SupportedCurrencies.XTZ, SupportedCurrencies.EUR),
        new Domain.Pair(SupportedCurrencies.ACA, SupportedCurrencies.EUR),
        new Domain.Pair(SupportedCurrencies.SGB, SupportedCurrencies.EUR),
        new Domain.Pair(SupportedCurrencies.MINA, SupportedCurrencies.EUR),
        new Domain.Pair(SupportedCurrencies.FLR, SupportedCurrencies.EUR),
        new Domain.Pair(SupportedCurrencies.DOT, SupportedCurrencies.EUR),
        new Domain.Pair(SupportedCurrencies.ATOM, SupportedCurrencies.EUR),
        new Domain.Pair(SupportedCurrencies.XLM, SupportedCurrencies.EUR),
    };

    public static IEnumerable<string> AsTickerSymbolList() =>
        _supportedPairs.Select(p => string.Join("/", p.BaseCurrency.TickerSymbol, p.QuoteCurrency.TickerSymbol));
    
    public static Pair? IsSupportedPair(string pairString) {
        var splitResult = pairString.Split("/", 2);
        var baseCurrencyTickerSymbol = splitResult[0];
        var quoteCurrencyTickerSymbol = splitResult[1];

        foreach(var pair in _supportedPairs) {
            if(pair.BaseCurrency.TickerSymbol == baseCurrencyTickerSymbol) {
                if(pair.QuoteCurrency.TickerSymbol == quoteCurrencyTickerSymbol) {
                    return pair;
                }
            }
        }

        return null;
    }
}