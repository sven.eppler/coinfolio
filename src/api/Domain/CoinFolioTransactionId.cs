namespace CoinFolio.Domain;

using Nanoid;

public class CoinFolioTransactionId {
    public static string Generate()
        => "cftx-" + Nanoid.Generate("abcdefghijklmnopqrstuvwxzyABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", 16);
}