namespace CoinFolio.Domain;

public record Currency(string Name, string TickerSymbol) {
    public override string ToString()
    {
        return $"{Name} ({TickerSymbol})";
    }
}
