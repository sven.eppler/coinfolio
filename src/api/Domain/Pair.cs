namespace CoinFolio.Domain;

public record Pair(Currency BaseCurrency, Currency QuoteCurrency) {};