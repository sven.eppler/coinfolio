namespace CoinFolio.Domain;

public static class Commands {
    public record Currency(string Name, string TickerSymbol);
    
    public record OpenCoinfolio(string CoinfolioId, string Title);

    public record CloseCoinfolio(string CoinfolioId, string OwnerToken, string LastWords);

    public record RecordBuyTransaction(string CoinFolioId, string OwnerToken, Pair Pair, decimal Amount, decimal UnitPrice, decimal Fee, DateTime MomentOfTransaction, string Exchange, string Notes);
    public record RecordSellTransaction(string CoinFolioId, string OwnerToken, Pair Pair, decimal Amount, decimal UnitPrice, decimal Fee, DateTime MomentOfTransaction, string Exchange, string Notes);
}
