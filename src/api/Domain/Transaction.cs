namespace CoinFolio.Domain;

public enum TransactionDirection
{
    BUY = 0,
    SELL = 1,
}

public record Transaction(string CoinFolioTransactionId, TransactionDirection Direction, Pair Pair, decimal Amount, decimal UnitPrice, decimal Fee, DateTime MomentOfTransaction, string Exchange, string Notes);
