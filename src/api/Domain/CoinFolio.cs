using Eventuous;

namespace CoinFolio.Domain;

public record CoinFolioId : AggregateId
{
    public CoinFolioId(string value) : base(value)
    {
        if(!Guid.TryParse(value, out _)) {
            throw new ArgumentException("CoinFolioId is not a valid UUIDv4!");
        }
    }
}

public class HoldingsEntry {
    public decimal Amount { get; set; } = default;
    public decimal TotalPaid { get; set; } = default;
}

public record CoinFolioState : AggregateState<CoinFolioState>
{
    // TODO remove the ID completely?
    public string Id { get; private set; } = String.Empty;
    public bool IsClosed { get; private set; } = false;
    public string OwnerToken { get; private set; } = String.Empty;
    public string Title { get; private set; } = String.Empty;
    public Dictionary<Currency, HoldingsEntry> Holdings { get; set; } = new Dictionary<Currency, HoldingsEntry>();
    public List<Transaction> Transactions { get; set; } = new List<Transaction>();
    public Dictionary<string, Transaction> KnownTransactions { get; set; } = new();


    internal class SimpleTransaction {
        public decimal Amount { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Fee { get; set; }

        public static implicit operator SimpleTransaction(Transaction tx) => new() {
            Amount = tx.Amount,
            UnitPrice = tx.UnitPrice,
            Fee = tx.Fee
        };
    }

    private readonly Dictionary<Currency, Queue<SimpleTransaction>> _unmatchedBuyTransactions = new();

    public CoinFolioState() {
        On<Events.CoinFolioOpened>((state, opened) => state with {
            Id = new CoinFolioId(opened.CoinFolioId),
            OwnerToken = opened.OwnerToken,
            Title = opened.Title,
        });

        On<Events.BuyTransactionRecorded>((state, bought) => {
            var buyCurrency = bought.Pair.BaseCurrency;
            if(!Holdings.ContainsKey(buyCurrency)) {
                Holdings[buyCurrency] = new();
            }

            Holdings[buyCurrency].Amount += bought.Amount;
            Holdings[buyCurrency].TotalPaid += bought.UnitPrice * bought.Amount + bought.Fee;

            var tx = new Transaction(bought.CoinFolioTransactionId, TransactionDirection.BUY, bought.Pair, bought.Amount, bought.UnitPrice, bought.Fee, bought.MomentOfTransaction, bought.Exchange ?? "", bought.Notes ?? "");
            Transactions.Add(tx);
            KnownTransactions.Add(tx.HashTransaction(), tx);

            if(!_unmatchedBuyTransactions.ContainsKey(bought.Pair.BaseCurrency)) {
                _unmatchedBuyTransactions[bought.Pair.BaseCurrency] = new();
            }

            _unmatchedBuyTransactions[bought.Pair.BaseCurrency].Enqueue(tx);

            return state;
        });

        On<Events.SellTransactionRecorded>((state, sold) => {
            var sellCurrency = sold.Pair.BaseCurrency;
            if(!Holdings.ContainsKey(sellCurrency)) {
                Holdings[sellCurrency] = new();
            }

            Holdings[sellCurrency].Amount -= sold.Amount;
            
            // FIFO matching of Buy-TXs vs. Sell-TX
            var remainingToSell = sold.Amount;
            var unmatchedTxList = _unmatchedBuyTransactions[sold.Pair.BaseCurrency];
            while(remainingToSell > 0) {
                var firstUnmatchedTx = unmatchedTxList.Peek();
                if(remainingToSell > firstUnmatchedTx.Amount)  {
                    // Eat up transaction
                    remainingToSell -= firstUnmatchedTx.Amount;
                    unmatchedTxList.Dequeue();
                }
                else {
                    firstUnmatchedTx.Fee = (remainingToSell / firstUnmatchedTx.Amount) * firstUnmatchedTx.Fee;
                    firstUnmatchedTx.Amount -= remainingToSell;
                    remainingToSell = 0;
                }
            }

            var totalPaid = 0m;
            foreach(var blubb in unmatchedTxList) {
                totalPaid += blubb.Amount * blubb.UnitPrice + blubb.Fee;
            }

            Holdings[sellCurrency].TotalPaid = totalPaid;

            var tx = new Transaction(sold.CoinFolioTransactionId, TransactionDirection.SELL, sold.Pair, sold.Amount, sold.UnitPrice, sold.Fee, sold.MomentOfTransaction, sold.Exchange ?? "", sold.Notes ?? "");
            Transactions.Add(tx);
            KnownTransactions.Add(tx.HashTransaction(), tx);

            return state;
        });

        On<Events.CoinFolioClosed>((state, closed) => state with { IsClosed = true });
    }
}

public class CoinFolio : Aggregate<CoinFolioState>
{
    public void OpenCoinFolio(string coinFolioId, string ownerToken, string title) {
        EnsureDoesntExist();
        // TODO validation
        if(string.IsNullOrWhiteSpace(title)) {
            throw new ArgumentException("title must be set", nameof(title));
        }

        Apply(new Events.CoinFolioOpened(coinFolioId, ownerToken, title));
    }

    public void RecordBuyTransaction(string ownerToken, Pair pair, decimal amount, decimal unitPrice, decimal fee, DateTime momentOfTransaction, string exchange, string notes) {
        EnsureExists();
        EnsureOpen();
        EnsureOwnerToken(ownerToken);
        // TODO validation

        var txHash = TransactionHasher.HashTransaction(TransactionDirection.BUY, pair, amount, unitPrice, fee, momentOfTransaction, exchange);
        if(State.KnownTransactions.ContainsKey(txHash)) {
            throw new DuplicatedTransactionException();
        }

        Apply(new Events.BuyTransactionRecorded(CoinFolioTransactionId.Generate(), pair, amount, unitPrice, fee, momentOfTransaction, exchange, notes));
    }

    public void RecordSellTransaction(string ownerToken, Pair pair, decimal amount, decimal unitPrice, decimal fee, DateTime momentOfTransaction, string exchange, string notes) {
        EnsureExists();
        EnsureOpen();
        EnsureOwnerToken(ownerToken);

        var txHash = TransactionHasher.HashTransaction(TransactionDirection.SELL, pair, amount, unitPrice, fee, momentOfTransaction, exchange);
        if(State.KnownTransactions.ContainsKey(txHash)) {
            throw new DuplicatedTransactionException();
        }
        
        if(State.Holdings.TryGetValue(pair.BaseCurrency, out var holding)) {
            if(holding.Amount < amount) {
                throw new Exceptions.CannotOverSellException(pair.BaseCurrency, amount, holding.Amount);
            }
        }
        else {
            throw new Domain.Exceptions.NotHoldingException(pair.BaseCurrency);
        }

        Apply(new Events.SellTransactionRecorded(CoinFolioTransactionId.Generate(), pair, amount, unitPrice, fee, momentOfTransaction, exchange, notes));
    }

    public void Close(string ownerToken, string lastWords) {
        EnsureExists();
        EnsureOpen();
        EnsureOwnerToken(ownerToken);

        Apply(new Events.CoinFolioClosed(State.Id, lastWords));
    }

    private void EnsureOpen() {
        if(this.State.IsClosed) {
            throw new Domain.Exceptions.CoinfolioClosedException($"The coinfolio with id={State.Id} is closed and cannot be changed.");
        }
    }

    private void EnsureOwnerToken(string ownerToken)
    {
        if(State.OwnerToken != ownerToken) {
            throw new InvalidOperationException("ownerToken mismatch!");
        }
    }
}
