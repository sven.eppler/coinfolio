using System.Globalization;
using System.Security.Cryptography;

namespace CoinFolio.Domain;

public static class TransactionHasher {
    public static string HashTransaction(TransactionDirection direction, Pair pair, decimal amount, decimal unitPrice, decimal fee, DateTime momentOfTransaction, string exchange) {
        var txString = String.Join("|", direction.ToString(), pair.ToString(), amount, unitPrice, fee, momentOfTransaction.ToString("o", CultureInfo.InvariantCulture), exchange);
        using(var sha256 = SHA256.Create()) {
            var hashBytes = sha256.ComputeHash(System.Text.Encoding.UTF8.GetBytes(txString));
            return Convert.ToHexString(hashBytes);
        }
    }

    public static string HashTransaction(this Transaction tx)
        => HashTransaction(tx.Direction, tx.Pair, tx.Amount, tx.UnitPrice, tx.Fee, tx.MomentOfTransaction, tx.Exchange);
}