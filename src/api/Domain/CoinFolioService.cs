using Eventuous;

namespace CoinFolio.Domain;

public class CoinFolioService : ApplicationService<CoinFolio, CoinFolioState, CoinFolioId>
{
    private static string Nanoid()
        => global::Nanoid.Nanoid.Generate("0123456789abcdefghijklmnopqrstuvwxyz", 32);

    public CoinFolioService(IAggregateStore store) : base(store)
    {
        OnNew<Commands.OpenCoinfolio>(
            cmd => new CoinFolioId(cmd.CoinfolioId),
            (coinfolio, cmd) => {
                coinfolio.OpenCoinFolio(cmd.CoinfolioId, Nanoid(), cmd.Title);
            }
        );

        OnExisting<Commands.CloseCoinfolio>(
            cmd => new CoinFolioId(cmd.CoinfolioId),
            (cf, cmd) => cf.Close(cmd.OwnerToken, cmd.LastWords)
        );

        OnExisting<Commands.RecordSellTransaction>(
            cmd => new CoinFolioId(cmd.CoinFolioId),
            (cf, cmd) => cf.RecordSellTransaction(cmd.OwnerToken, cmd.Pair, cmd.Amount, cmd.UnitPrice, cmd.Fee, cmd.MomentOfTransaction, cmd.Exchange, cmd.Notes)
        );

        OnExisting<Commands.RecordBuyTransaction>(
            cmd => new CoinFolioId(cmd.CoinFolioId),
            (cf, cmd) => cf.RecordBuyTransaction(cmd.OwnerToken, cmd.Pair, cmd.Amount, cmd.UnitPrice, cmd.Fee, cmd.MomentOfTransaction, cmd.Exchange, cmd.Notes)
        );


    }
}
