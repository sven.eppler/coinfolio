using Eventuous;

namespace CoinFolio.Domain;

public static class Events {
    [EventType("Opened")]
    public record CoinFolioOpened(string CoinFolioId, string OwnerToken, string Title);
    [EventType("BuyTransactionRecorded")]
    public record BuyTransactionRecorded(string CoinFolioTransactionId, Pair Pair, decimal Amount, decimal UnitPrice, decimal Fee, DateTime MomentOfTransaction, string Exchange, string Notes);
    [EventType("SellTransactionRecorded")]
    public record SellTransactionRecorded(string CoinFolioTransactionId, Pair Pair, decimal Amount, decimal UnitPrice, decimal Fee, DateTime MomentOfTransaction, string Exchange, string Notes);
    [EventType("Closed")]
    public record CoinFolioClosed(string coinFolioId, string LastWords);
}