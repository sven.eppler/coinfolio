using System.Runtime.Serialization;

namespace CoinFolio.Domain;

[Serializable]
public class DuplicatedTransactionException : Exception
{
    public DuplicatedTransactionException() : base("the transaction has been detected as duplicate")
    {
    }

    public DuplicatedTransactionException(string? message) : base(message)
    {
    }

    public DuplicatedTransactionException(string? message, Exception? innerException) : base(message, innerException)
    {
    }

    protected DuplicatedTransactionException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }
}