using System.Text.Json.Serialization;

using EventStore.Client;

using Eventuous.EventStore;

using InfluxDB.Client;

using CoinFolio.LokiLogging;
using Prometheus;
using CoinFolio.Services;

public class Program {
    public static void Main(string[] args) {
        var builder = WebApplication.CreateBuilder(args);

        builder.Logging.AddOpenTelemetryLogging(builder.Configuration);
        
        builder.Services
            .AddSingleton<EventStoreClient>((provider) => {
                var config = provider.GetRequiredService<IConfiguration>();
                var esdbSettings = EventStoreClientSettings.Create(config.GetConnectionString("esdb"));
                return new EventStoreClient(esdbSettings);
            })
            .AddAggregateStore<EsdbEventStore>();

        builder.Services.AddControllers();
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();
        builder.Services.AddApplicationService<CoinFolio.Domain.CoinFolioService, CoinFolio.Domain.CoinFolio>();
        builder.Services.AddSingleton<IInfluxDBClient, InfluxDBClient>((serviceProvider) => {
            var config = builder.Configuration;
            return new InfluxDBClient(config.GetValue<string>("influxdb:address"), config.GetValue<string>("influxdb:accesstoken"));
        });

        builder.Services.Configure<Microsoft.AspNetCore.Mvc.JsonOptions>(o => o.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));
        builder.Services.AddMemoryCache();
        builder.Services.AddHttpClient<IKrakenPairService>();
        builder.Services.AddTransient<IKrakenPairService, KrakenPairService>();


        Eventuous.TypeMap.RegisterKnownEventTypes();

        var app = builder.Build();

        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
            app.UseCors(builder =>
            {
                builder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin();
            });
        }
        else {
            app.UseCors(builder =>
            {
                builder.AllowAnyHeader().AllowAnyMethod().WithOrigins(app.Configuration.GetValue<string>("FrontendOrigin"));
            });
        }

        app.UseAuthorization();

        app.UseHttpMetrics();

        app.UseRouting();

        app.UseEndpoints(routeBuilder => {
            routeBuilder.MapMetrics();
        });

        app.MapControllers();

        app.Run();
    }
}