using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace CoinFolio;

public class DomainExceptionFilter : IExceptionFilter
{
    public void OnException(ExceptionContext context)
    {
        switch (context.Exception) {
            case Domain.Exceptions.CoinfolioClosedException : {
                context.HttpContext.Response.StatusCode = 409;
                context.Result = new JsonResult(new { Message = "coinfolio already closed"});
                break;
            }
            case Eventuous.AggregateNotFoundException : {
                context.HttpContext.Response.StatusCode = 404;
                context.Result = new JsonResult(new { Message = "coinfolio not found"});
                break;
            }
            case Domain.Exceptions.CannotOverSellException ex : {
                context.HttpContext.Response.StatusCode = 409;
                context.Result = new JsonResult(new { Message = ex.Message});
                break;
            }

            case Domain.Exceptions.NotHoldingException ex : {
                context.HttpContext.Response.StatusCode = 409;
                context.Result = new JsonResult(new { Message = ex.Message});
                break;
            }
        }
    }
}