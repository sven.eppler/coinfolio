using System.Text.Json;

namespace CoinFolio.Services;

public record Pair(string BaseCurrency, string QuoteCurrency) {
    public string TickerSymbol { get { return BaseCurrency + "/" + QuoteCurrency; } }
};

public interface IKrakenPairService {
    Task<IEnumerable<Pair>> GetPairs();
}


public class KrakenPairService : IKrakenPairService {
    private readonly HttpClient _client;
    private readonly Dictionary<string, string> _currencyMap = new Dictionary<string, string>() {
        { "ZEUR", "EUR" },
        { "ZUSD", "USD" },
        { "XBT", "BTC" },
        { "XXDG", "DOGE" },
        { "XETH", "ETH" },
        { "XET", "ETC" },
        { "XLTC", "LTC" },
        { "XMLN", "MLN" },
        { "XREP", "REP" },
        { "XXBT", "BTC" },
        { "XXLM", "XLM" },
        { "XXMR", "XMR" },
        { "XXRP", "XRP" },
        { "XZEC", "ZEC" },
        { "LUNA", "LUNAC" },
        { "LUNA2", "LUNA" },
    };

    public KrakenPairService(HttpClient client)
    {
        _client = client;
    }

    public string convertCurrency(string input) {
        input = input.ToUpper();

        if(_currencyMap.TryGetValue(input, out var mappedCurrency)) {
            return mappedCurrency;
        }

        return input;
    }

    public async Task<IEnumerable<Pair>> GetPairs()
    {
        var res = await _client.GetAsync("https://api.kraken.com/0/public/AssetPairs");
        
        if(res.StatusCode != System.Net.HttpStatusCode.OK) {
            throw new Exception("Failed to retrieve tradable AssetPairs from kraken api!");
        }

        var body = await res.Content.ReadAsStringAsync();

        var root = JsonDocument.Parse(body);
        var resultJson =  root.RootElement.GetProperty("result");

        var result = new List<Pair>();

        foreach(var prop in resultJson.EnumerateObject()) {
            var pair = resultJson.GetProperty(prop.Name);

            if(!pair.TryGetProperty("quote", out var quoteCurrencyElement)) {
                continue;
            }

            if(!pair.TryGetProperty("base", out var baseCurrencyElement)) {
                continue;
            }

            var baseCurrency = convertCurrency(baseCurrencyElement.GetString());
            var quoteCurrency = convertCurrency(quoteCurrencyElement.GetString());

            result.Add(new Pair(baseCurrency, quoteCurrency));
        }

        return result;
    }
}