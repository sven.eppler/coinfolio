using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace ApiIntegrationTests;

public static class HttpResponseMessageExtensions {
    public static async Task<T?> JSONAsync<T>(this HttpResponseMessage msg) {
        var jsonString = await msg.Content.ReadAsStringAsync();

        return System.Text.Json.JsonSerializer.Deserialize<T>(jsonString, new JsonSerializerOptions(JsonSerializerDefaults.Web));
    }
}
