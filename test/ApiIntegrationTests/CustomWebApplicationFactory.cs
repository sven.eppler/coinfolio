using System.Collections.Generic;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;

namespace ApiIntegrationTests;

public class CustomWebApplicationFactory<TProgram>
    : WebApplicationFactory<TProgram> where TProgram : class
{
    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        var data = new Dictionary<string, string> {
                { "ConnectionStrings:esdb", "esdb://admin:changeit@localhost:2113?tls=false" },
            };
        
        builder.ConfigureAppConfiguration(builder => {
            builder.AddInMemoryCollection(data);
        });
    }
}
