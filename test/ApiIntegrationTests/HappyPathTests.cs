using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Threading.Tasks;

using Xunit;

namespace ApiIntegrationTests;


public class HappyPathTests : IClassFixture<CustomWebApplicationFactory<Program>>
{
    internal record CreateCoinfolioResponse(string Id, string OwnerToken);
    internal record Currency(string Name, string TickerSymbol);
    internal record QueryHoldingsEntry(Currency Currency, decimal Holdings, decimal CurrentValue);
    private readonly CustomWebApplicationFactory<Program> _factory;

    public HappyPathTests(CustomWebApplicationFactory<Program> factory)
    {
        _factory = factory;
    }

    [Fact(Skip = "Disabled as a test, since it requires a real EventStore and InfluxDB")]
    public async Task HappyPathAsync()
    {
        var client = _factory.CreateClient();
        var content = JsonContent.Create(new {
            Title = "Integration Test " + Guid.NewGuid(),
        });

        var response = await client.PostAsync("/commands/open-coinfolio", content);
        response.EnsureSuccessStatusCode();

        var createCoinfolioResponse = await response.JSONAsync<CreateCoinfolioResponse>();
        Assert.NotNull(createCoinfolioResponse);

        var ownerToken = createCoinfolioResponse!.OwnerToken;
        var id = createCoinfolioResponse.Id;

        response = await client.PostAsJsonAsync("/commands/record-buy-transaction", new {
            coinfolioId = id,
            ownerToken = ownerToken,
            pair = "BTC/EUR",
            amount = 15,
            unitPrice = 10,
            fee = 0.123,
            momentOfTransaction = DateTime.UtcNow,
        });

        response.EnsureSuccessStatusCode();

        response = await client.PostAsJsonAsync("/commands/record-sell-transaction", new {
            coinfolioId = id,
            ownerToken = ownerToken,
            pair = "BTC/EUR",
            amount = 5,
            unitPrice = 10,
            fee = 0.123,
            momentOfTransaction = DateTime.UtcNow,
        });

        response.EnsureSuccessStatusCode();

        response = await client.PostAsJsonAsync("/queries/holdings", new {
            coinfolioId = id,
            ownerToken = ownerToken,
        });

        response.EnsureSuccessStatusCode();

        var holdings = await response.JSONAsync<List<QueryHoldingsEntry>>();

        Assert.Single(holdings);
        var firstHoldings = holdings!.First();

        Assert.Equal(10m, firstHoldings.Holdings);

        response = await client.PostAsJsonAsync("/commands/close-coinfolio", new {
            ownerToken,
            coinfolioId = id,
            lastWords = "farewell!"
        });

        response.EnsureSuccessStatusCode();

        response = await client.PostAsJsonAsync("/commands/record-buy-transaction", new {
            coinfolioId = id,
            ownerToken = ownerToken,
            pair = "BTC/EUR",
            amount = 15,
            unitPrice = 10,
            fee = 0.123,
            momentOfTransaction = DateTime.UtcNow,
        });

        Assert.Equal(System.Net.HttpStatusCode.BadRequest, response.StatusCode);
    }
}