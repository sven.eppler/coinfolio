using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

using RichardSzalay.MockHttp;
using Xunit;

using CoinFolio.Services;

namespace CoinFolio.Tests;

public class KrakenPairServiceTests {
    [Fact]
    public async Task GetEuroPairsWorks() {
        var json = @"
            {
                ""errors"": [],
                ""result"": {
                    ""ADAEUR"": {
                        ""base"": ""ADA"",
                        ""quote"": ""EUR""
                    }
                }
            }";
        var mockClient = new MockHttpMessageHandler();
        mockClient
            .When("https://api.kraken.com/0/public/AssetPairs")
            .Respond(HttpStatusCode.OK, new StringContent(json));

        var client = mockClient.ToHttpClient();

        var service = new KrakenPairService(client);
        var result = await service.GetPairs();

        Assert.Single(result);
        var firstEntry = result.First();

        Assert.Equal("ADA", firstEntry.BaseCurrency);
        Assert.Equal("EUR", firstEntry.QuoteCurrency);
    }

    [Fact]
    public async Task GetEuroPairsReturnsEmptyListOnEmptyResult() {
        var json = @"
            {
                ""errors"": [],
                ""result"": {
                }
            }";
        var mockClient = new MockHttpMessageHandler();
        mockClient
            .When("https://api.kraken.com/0/public/AssetPairs")
            .Respond(HttpStatusCode.OK, new StringContent(json));

        var client = mockClient.ToHttpClient();

        var service = new KrakenPairService(client);
        var result = await service.GetPairs();

        Assert.Empty(result);
    }

    [Fact]
    public async Task ConvertsCurrencies() {
        var json = @"
            {
                ""errors"": [],
                ""result"": {
                    ""BTCEUR"": {
                        ""base"": ""XBT"",
                        ""quote"": ""ZEUR""
                    }
                }
            }";
        var mockClient = new MockHttpMessageHandler();
        mockClient
            .When("https://api.kraken.com/0/public/AssetPairs")
            .Respond(HttpStatusCode.OK, new StringContent(json));

        var client = mockClient.ToHttpClient();

        var service = new KrakenPairService(client);
        var result = await service.GetPairs();

        Assert.Single(result);
        var firstEntry = result.First();
        Assert.Equal("BTC", firstEntry.BaseCurrency);
        Assert.Equal("EUR", firstEntry.QuoteCurrency);
    }

    [Fact]
    public async Task GetEuroPairs_SkipsOnMissingBaseCurrencies()
    {
        var json = @"
            {
                ""errors"": [],
                ""result"": {
                    ""NoBase"": {
                        ""quote"": ""ZEUR""
                    }
                }
            }";
        var mockClient = new MockHttpMessageHandler();
        mockClient
            .When("https://api.kraken.com/0/public/AssetPairs")
            .Respond(HttpStatusCode.OK, new StringContent(json));

        var client = mockClient.ToHttpClient();

        var service = new KrakenPairService(client);
        var result = await service.GetPairs();

        Assert.Empty(result);
    }

    [Fact]
    public async Task GetEuroPairs_SkipsOnMissingQuoteCurrencies()
    {
        var json = @"
            {
                ""errors"": [],
                ""result"": {
                    ""NoQuote"": {
                        ""base"": ""ZEUR""
                    }
                }
            }";
        var mockClient = new MockHttpMessageHandler();
        mockClient
            .When("https://api.kraken.com/0/public/AssetPairs")
            .Respond(HttpStatusCode.OK, new StringContent(json));

        var client = mockClient.ToHttpClient();

        var service = new KrakenPairService(client);
        var result = await service.GetPairs();

        Assert.Empty(result);
    }
}