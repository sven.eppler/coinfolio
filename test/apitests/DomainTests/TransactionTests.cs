using System;

using CoinFolio.Domain;

using Xunit;

namespace CoinFolio.Tests.DomainTests;

public class TransactionTests {
    [Fact]
    public void TransactionsHaveHashes() {
        var btc = new Domain.Currency("BTC", "BTC");
        var eur = new Domain.Currency("EUR", "EUR");
        var now = DateTime.UtcNow;
        var tx = new Domain.Transaction(Domain.CoinFolioTransactionId.Generate(), Domain.TransactionDirection.BUY, new Domain.Pair(btc, eur), 1m, 1m, 1m, now, "Kraken", "Notes");

        var hash = tx.HashTransaction();

        Assert.NotNull(hash);
        Assert.Equal(64, hash.Length);
    }

    [Fact]
    public void TansactionWithIdenticalDataHasIdenticalHash() {
        var btc = new Domain.Currency("BTC", "BTC");
        var eur = new Domain.Currency("EUR", "EUR");
        var now = DateTime.UtcNow;
        var tx1 = new Domain.Transaction(Domain.CoinFolioTransactionId.Generate(), Domain.TransactionDirection.BUY, new Domain.Pair(btc, eur), 1m, 1m, 1m, now, "Kraken", "Notes");
        var tx2 = new Domain.Transaction(Domain.CoinFolioTransactionId.Generate(), Domain.TransactionDirection.BUY, new Domain.Pair(btc, eur), 1m, 1m, 1m, now, "Kraken", "Notes");

        Assert.Equal(tx1.HashTransaction(), tx2.HashTransaction());
    }
}