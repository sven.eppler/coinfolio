using System;
using System.Linq;

using CoinFolio.Domain;

using Xunit;

namespace CoinFolio.Tests.DomainTests;

public class CoinFolioAccountTests
{
    private static Currency bitcoin = new Currency("Bitcoin", "BTC");
    private static Currency litecoin = new Currency("Litecoin", "LTC");
    private static Currency euro = new Currency("Euro", "EUR");

    private static Pair BtcEur = new Pair(bitcoin, euro);
    private static Pair LtcEur = new Pair(litecoin, euro);

    private static Domain.CoinFolio EmptyCoinfolio()
    {
        var emptyCoinfolio = new Domain.CoinFolio();
        emptyCoinfolio.OpenCoinFolio(Guid.NewGuid().ToString(), "dummy-token", "My Coinfolio");
        return emptyCoinfolio;
    }

    [Fact]
    public void HappyPath()
    {
        var coinFolio = new CoinFolio.Domain.CoinFolio();
        Assert.Empty(coinFolio.Changes);
        var expectedCoinfolioId = Guid.NewGuid().ToString();
        var ownerToken = "ownerToken";

        coinFolio.OpenCoinFolio(expectedCoinfolioId, ownerToken, "My Coinfolio");
        Assert.Equal("My Coinfolio", coinFolio.State.Title);
        Assert.Single(coinFolio.Changes);
        var lastEvent = coinFolio.Changes.Last();

        var opened = Assert.IsType<CoinFolio.Domain.Events.CoinFolioOpened>(lastEvent);
        Assert.Equal(expectedCoinfolioId, opened.CoinFolioId);
        Assert.Equal("ownerToken", opened.OwnerToken);

        var momentOfBuyTransaction = DateTime.Parse("2011-03-01 12:00:00");
        coinFolio.RecordBuyTransaction(ownerToken, BtcEur, 10, 150, 0.33m, momentOfBuyTransaction, "DummyExchange", "DummyNote");
        Assert.Equal(expectedCoinfolioId, coinFolio.State.Id);
        Assert.Equal(2, coinFolio.Changes.Count());

        lastEvent = coinFolio.Changes.Last();
        var bought = Assert.IsType<Events.BuyTransactionRecorded>(lastEvent);
        Assert.Equal(bitcoin, bought.Pair.BaseCurrency);
        Assert.Equal(10, bought.Amount);
        Assert.Equal(150, bought.UnitPrice);
        Assert.Equal(0.33m, bought.Fee);
        Assert.Equal(momentOfBuyTransaction, bought.MomentOfTransaction);
        Assert.Equal(10, coinFolio.State.Holdings[bitcoin].Amount);

        var momentOfSellTransaction = DateTime.Parse("2011-04-01 12:00:00");
        coinFolio.RecordSellTransaction(ownerToken, BtcEur, 5, 200, 0.12m, momentOfSellTransaction, "DummyExchange", "DummyNote");
        Assert.Equal(3, coinFolio.Changes.Count());

        lastEvent = coinFolio.Changes.Last();
        var sold = Assert.IsType<Events.SellTransactionRecorded>(lastEvent);
        Assert.Equal(bitcoin, sold.Pair.BaseCurrency);
        Assert.Equal(5, sold.Amount);
        Assert.Equal(200, sold.UnitPrice);
        Assert.Equal(0.12m, sold.Fee);
        Assert.Equal(momentOfSellTransaction, sold.MomentOfTransaction);
        Assert.Equal(5, coinFolio.State.Holdings[bitcoin].Amount);
    }

    [Fact]
    public void CannotOverSell()
    {
        var coinfolio = EmptyCoinfolio();

        coinfolio.RecordBuyTransaction(coinfolio.State.OwnerToken, BtcEur, 1, 10, 0, DateTime.UtcNow, "DummyExchange", "DummyNote");

        var ex = Assert.Throws<Domain.Exceptions.CannotOverSellException>(() => coinfolio.RecordSellTransaction(coinfolio.State.OwnerToken, BtcEur, 2, 10, 0, DateTime.UtcNow, "DummyExchange", "DummyNote"));
    }

    [Fact]
    public void CannotSellWhatHasNeverBeenBought()
    {
        var coinfolio = EmptyCoinfolio();

        var ex = Assert.Throws<Domain.Exceptions.NotHoldingException>(() => coinfolio.RecordSellTransaction(coinfolio.State.OwnerToken, LtcEur, 2, 10, 0, DateTime.UtcNow, "DummyExchange", "DummyNote"));
    }

    [Theory]
    [InlineData(null)]
    [InlineData("")]
    [InlineData(" ")]
    [InlineData("\t")]
    public void NoEmptyTitle(string invalidTitle)
    {
        var coinfolio = new Domain.CoinFolio();

        Assert.Throws<ArgumentException>(() => coinfolio.OpenCoinFolio("dummyId", "dummyToken", invalidTitle));
    }

    [Fact]
    public void BuyAssetPreservesOldState()
    {
        var coinfolio = EmptyCoinfolio();
        var expectedId = coinfolio.State.Id;
        var expectedTitle = coinfolio.State.Title;
        var expectedOwnerToken = coinfolio.State.OwnerToken;

        Assert.False(coinfolio.State.Holdings.ContainsKey(bitcoin));

        var momentOfBuy = DateTime.Parse("2011-03-01 12:00:00");
        coinfolio.RecordBuyTransaction(coinfolio.State.OwnerToken, BtcEur, 10, 150, 0, momentOfBuy, "DummyExchange", "DummyNote");
        Assert.Equal(10, coinfolio.State.Holdings[bitcoin].Amount);

        coinfolio.RecordBuyTransaction(coinfolio.State.OwnerToken, BtcEur, 10, 150, 0, momentOfBuy.AddDays(1), "DummyExchange", "DummyNote");
        Assert.Equal(20, coinfolio.State.Holdings[bitcoin].Amount);

        Assert.Equal(expectedId, coinfolio.State.Id);
        Assert.Equal(expectedTitle, coinfolio.State.Title);
        Assert.Equal(expectedOwnerToken, coinfolio.State.OwnerToken);
    }

    [Fact]
    public void CoinFolioThrowsOnDuplicatedBuyTransactions() {
        var coinfolio = EmptyCoinfolio();

        var momentOfTransaction = DateTime.Parse("2011-03-01 12:00:00");
        coinfolio.RecordBuyTransaction(coinfolio.State.OwnerToken, BtcEur, 10, 10, 10, momentOfTransaction, "SomeExchange", "SomeNotes");
        Assert.Throws<DuplicatedTransactionException>(() => coinfolio.RecordBuyTransaction(coinfolio.State.OwnerToken, BtcEur, 10, 10, 10, momentOfTransaction, "SomeExchange", "SomeNotes"));
    }

    [Fact]
    public void CoinFolioThrowsOnDuplicatedSellTransactions() {
        var coinfolio = EmptyCoinfolio();
        var momentOfTransaction = DateTime.Parse("2011-03-01 12:00:00");

        // Add some Bitcoin to sell them.
        coinfolio.RecordBuyTransaction(coinfolio.State.OwnerToken, BtcEur, 100, 10, 10, momentOfTransaction, "SomeExchange", "SomeNotes");

        coinfolio.RecordSellTransaction(coinfolio.State.OwnerToken, BtcEur, 10, 10, 10, momentOfTransaction, "SomeExchange", "SomeNotes");
        Assert.Throws<DuplicatedTransactionException>(() => coinfolio.RecordSellTransaction(coinfolio.State.OwnerToken, BtcEur, 10, 10, 10, momentOfTransaction, "SomeExchange", "SomeNotes"));
    }

    [Fact]
    public void CalculatingTotalPaidCorrectly() {
        var coinfolio = EmptyCoinfolio();
        var momentOfTransaction = DateTime.Parse("2011-03-01 12:00:00");

        coinfolio.RecordBuyTransaction(coinfolio.State.OwnerToken, BtcEur, 10, 10, 0, momentOfTransaction, "SomeExchange", "SomeNotes");

        var btcHolding = coinfolio.State.Holdings[bitcoin];
        Assert.Equal(10, btcHolding.Amount);
        Assert.Equal(100, btcHolding.TotalPaid);

        coinfolio.RecordSellTransaction(coinfolio.State.OwnerToken, BtcEur, 2, 20, 0, momentOfTransaction, "SomeExchange", "SomeNotes");

        Assert.Equal(8, btcHolding.Amount);
        Assert.Equal(80, btcHolding.TotalPaid);

        coinfolio.RecordSellTransaction(coinfolio.State.OwnerToken, BtcEur, 2, 40, 0, momentOfTransaction, "SomeExchange", "SomeNotes");

        Assert.Equal(6, btcHolding.Amount);
        Assert.Equal(60, btcHolding.TotalPaid);
    }

    [Fact]
    public void CalculatingTotalPaidCorrectly_MultipleBuyTransactions() {
        var coinfolio = EmptyCoinfolio();
        var momentOfTransaction = DateTime.Parse("2011-03-01 12:00:00");

        coinfolio.RecordBuyTransaction(coinfolio.State.OwnerToken, BtcEur, 5, 10, 0, momentOfTransaction, "SomeExchange", "SomeNotes");
        coinfolio.RecordBuyTransaction(coinfolio.State.OwnerToken, BtcEur, 5, 20, 0, momentOfTransaction, "SomeExchange", "SomeNotes");
        coinfolio.RecordBuyTransaction(coinfolio.State.OwnerToken, BtcEur, 5, 30, 0, momentOfTransaction, "SomeExchange", "SomeNotes");

        var btcHolding = coinfolio.State.Holdings[bitcoin];
        Assert.Equal(15, btcHolding.Amount);
        Assert.Equal(300, btcHolding.TotalPaid);

        coinfolio.RecordSellTransaction(coinfolio.State.OwnerToken, BtcEur, 5, 20, 0, momentOfTransaction, "SomeExchange", "SomeNotes");

        Assert.Equal(10, btcHolding.Amount);
        Assert.Equal(250, btcHolding.TotalPaid);

        coinfolio.RecordSellTransaction(coinfolio.State.OwnerToken, BtcEur, 7, 40, 0, momentOfTransaction, "SomeExchange", "SomeNotes");

        Assert.Equal(3, btcHolding.Amount);
        Assert.Equal(90, btcHolding.TotalPaid);
    }

    [Fact]
    public void CalculatingTotalPaidCorrectly_ConsiderFees() {
        var coinfolio = EmptyCoinfolio();
        var momentOfTransaction = DateTime.Parse("2011-03-01 12:00:00");

        coinfolio.RecordBuyTransaction(coinfolio.State.OwnerToken, BtcEur, 10, 10, 10, momentOfTransaction, "SomeExchange", "SomeNotes");

        var btcHolding = coinfolio.State.Holdings[bitcoin];
        Assert.Equal(10, btcHolding.Amount);
        Assert.Equal(110, btcHolding.TotalPaid);

        coinfolio.RecordSellTransaction(coinfolio.State.OwnerToken, BtcEur, 5, 20, 5, momentOfTransaction, "SomeExchange", "SomeNotes");

        Assert.Equal(5, btcHolding.Amount);
        Assert.Equal(55, btcHolding.TotalPaid);
    }
}