#!/bin/bash

set -e -x

GIT_VERSION=`git describe --exact-match`

echo "GIT_VERSION=${GIT_VERSION}"

export COINFOLIO_API_IMAGE_VERSION=${GIT_VERSION}
export COINFOLIO_FRONTEND_IMAGE_VERSION=${GIT_VERSION}
export COINFOLIO_LIVEQUOTESSERVICE_IMAGE_VERSION=${GIT_VERSION}

docker-compose pull
docker-compose up -d